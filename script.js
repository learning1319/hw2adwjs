/* якщо ми очікуємо певну інформацію від користувача, яка впливатиме на виконання
нашого коду, наприклад: валідація введених даних у форму, може викливаи помилку і зупинити код,
або може бути помилка під час парсингу файлу .json, отриманого з сервера.
Try/catch допоможе уникнути зупинки коду і отримати сповіщення про помилку з виведенням
в консоль або користувачу.

*/


const requiredProperties = ['author', 'name', 'price'];
const divRoot = document.getElementById('root');
const list = document.createElement('ul');

divRoot.append(list);

console.log(divRoot);

function validator(object, props){
    for(let i = 0; i < props.length; i++){
       if(!object.hasOwnProperty(props[i])){
         return false
      } 
    }
    return true;
}


function listMaker(arr){
    arr.map((item)=>{
        try{ 
        if(validator(item, requiredProperties)){
            let newListItem = document.createElement("li");
            newListItem.textContent = `Author: ${item.author}, Name: ${item.name}, Price: ${item.price}`;
            list.append(newListItem)
        } else{
            throw new Error('Have no property')
        }
    } catch(err){
        console.log(err)
    }
}
    )
}



const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

listMaker(books);